@ECHO OFF
:: Print the starting time
FOR /F "tokens=1-2 delims=/:" %%a IN ("%TIME%") DO (SET mytime=%%a:%%b)
ECHO Started at %mytime%
:: Optional parameter, defaults to 3,5 hours
SET nap_duration=222
IF NOT "%1"=="" SET nap_duration=%1
ECHO Waking you up in %nap_duration% minutes!
:: Convert min to secs
SET /A nap_duration=60*%nap_duration%
ECHO Go for a nap!
:: Ping shold be the same as *nix "sleep"
:: In this case I started VLC as alarm, you can use your imagination here
PING 127.0.0.1 -n %nap_duration% > NUL && (ECHO WAKE UP! && "C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" C:\alarm.mp3)